public class Vector 
{
    private double i;
    private double j;
    private double k;
    
    public Vector()
    {
        this.i = 0;
        this.j = 0;
        this.k = 0;
    }
    
    public Vector(double i, double j, double k)
    {
        this.i = i;
        this.j = j;
        this.k = k;
    }

    public double geti() 
    {
        return i;
    }

    public void setiPos(double i) 
    {
        this.i = i;
    }

    public double getj() 
    {
        return j;
    }

    public void setjPos(double j) 
    {
        this.j = j;
    }

    public double getk() 
    {
        return k;
    }

    public void setkPos(double k) 
    {
        this.k = k;
    }

    @Override
    public String toString() 
    {
        return "( " +  i + " | " + j + " | " + k +  " )";
    }
}
