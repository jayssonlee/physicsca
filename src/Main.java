import java.util.InputMismatchException;
import java.util.Scanner;

public class Main 
{
    public static void main(String[] args)
    { 
        //constants throughout the simulation
        double radiusOfBall = 0.05;
        double densityOfBall = 7800;
        double densityOfFluid = 80;
        double dragCoef = 0.1;
        
        //constant vectors
        Vector wind = new Vector(2, 3, 0);
        Vector spinPseudoVector = new Vector( (double)-10 / 3, (double)-5 / 3, (double)10 / 3);
        
        //changing vectors
        Vector initialPosition = new Vector(2, -3, 6);
        Vector initialVelocity = new Vector(-5, 14, 2);
        
        /*--------------------------------------------------------------------*/
        
        Scanner in = new Scanner(System.in);
        
        System.out.println("Game Physics CA:");
        
        System.out.println("Menu:");
        System.out.println("Press 1: To show the current value(s) of variables.");
        System.out.println("Press 2: To change the variables' value.");
        System.out.println("Press 3: Comfirm and start the simulation");
        System.out.println("Press 4: To Exit");
        
        try
        {
            boolean end_program = false;
            
            while(end_program == false)
            {
                System.out.print("\nInput: ");
                int i = in.nextInt();

                if(i > 0 && i < 5)
                {
                    switch(i)
                    {
                        case 1:
                            System.out.println("Vectors:");
                            System.out.println("Initial Position: " + initialPosition);
                            System.out.println("Initial Velocity: " + initialVelocity);
                            System.out.println("Wind: " + wind);
                            System.out.println("Spin pseudo-vector: " + spinPseudoVector);
                            System.out.println("\nVariables:");
                            System.out.println("Radius of ball(object): " + radiusOfBall);
                            System.out.println("Density of ball: " + densityOfBall);
                            System.out.println("Density of fluid: " + densityOfFluid);
                            System.out.println("Cooefficient of drag: " + dragCoef);
                            break;

                        case 2:
                            System.out.println("To change certain parameters: input the corresponding number");
                            System.out.println("1. Initial Position");
                            System.out.println("2. Initial Velocity");
                            System.out.println("3. Wind");
                            System.out.println("4. Spin pseudo-vector");
                            System.out.println("5. Radius of ball");
                            System.out.println("6. Density of ball");
                            System.out.println("7. Density of fluid");
                            System.out.println("8. Cooefficient of drag");
                            System.out.println("\n0 to exit");

                            boolean end_input = false;

                            while(end_input == false)
                            {
                                System.out.print("Input: ");
                                int choice = in.nextInt();

                                if(choice == 0)
                                {
                                    end_input = true;
                                    System.out.println("Ending changing parameter...");
                                }
                                else if(choice > 0 && choice < 9)
                                {
                                    switch(choice)
                                    {
                                        case 1:
                                            System.out.println("New Position: (i, j, k) value followed by enter");
                                            double newPI = (double)in.nextDouble();
                                            double newPJ = (double)in.nextDouble();
                                            double newPK = (double)in.nextDouble();
                                            initialPosition = new Vector(newPI, newPJ, newPK);
                                            System.out.println("New position inserted.");
                                            break;

                                        case 2:
                                            System.out.println("New Velocity: (i, j, k) value followed by enter");
                                            double newVI = (double)in.nextDouble();
                                            double newVJ = (double)in.nextDouble();
                                            double newVK = (double)in.nextDouble();
                                            initialVelocity = new Vector(newVI, newVJ, newVK);
                                            System.out.println("New velocity inserted.");
                                            break;

                                        case 3:
                                            System.out.println("New Wind: (i, j, k) value followed by enter");
                                            double newWI = (double)in.nextDouble();
                                            double newWJ = (double)in.nextDouble();
                                            double newWK = (double)in.nextDouble();
                                            wind = new Vector(newWI, newWJ, newWK);
                                            System.out.println("New wind inserted.");
                                            break;

                                        case 4:
                                            System.out.println("New Spin vector: (i, j, k) value followed by enter");
                                            double newSI = (double)in.nextDouble();
                                            double newSJ = (double)in.nextDouble();
                                            double newSK = (double)in.nextDouble();
                                            wind = new Vector(newSI, newSJ, newSK);
                                            System.out.println("New spin vector inserted.");
                                            break;

                                        case 5:
                                            System.out.println("New radius:");
                                            radiusOfBall = in.nextDouble();
                                            System.out.println("New radius inserted.");
                                            break;

                                        case 6:
                                            System.out.println("New density of ball:");
                                            densityOfBall = in.nextDouble();
                                            System.out.println("New density of ball inserted.");
                                            break;

                                        case 7:
                                            System.out.println("New density of fluid:");
                                            densityOfFluid = in.nextDouble();
                                            System.out.println("New density of fluid inserted.");
                                            break;

                                        case 8:
                                            System.out.println("New coefficient of drag:");
                                            dragCoef = in.nextDouble();
                                            System.out.println("New cooefficient inserted.");
                                            break;
                                    }
                                }
                                else
                                {
                                    System.out.println("Wrong input");
                                    choice = 0;
                                    in.nextLine();
                                }
                            }
                        break;

                        case 3:
                            //derived variables
                            double ballSurfaceArea = Math.PI * radiusOfBall * radiusOfBall;
                            double massOfBall = Utilities.getSphereMass(radiusOfBall, densityOfBall);

                            //constant force
                            Vector gForce = Utilities.getForceOfGravity(massOfBall);

                            //initializing euler's
                            Vector apparentVelocity = Utilities.minusVector(initialVelocity, wind);

                            Vector dragForce = Utilities.getForceOfDrag(densityOfFluid, ballSurfaceArea, dragCoef, apparentVelocity);
                            System.out.println(dragForce);
                            Vector magnusForce = Utilities.getMagnusForce(densityOfFluid, ballSurfaceArea, radiusOfBall, apparentVelocity, spinPseudoVector);
                            System.out.println(magnusForce);
                            Vector netForce = Utilities.addVector(Utilities.addVector(gForce, dragForce), magnusForce);
                            Vector acceleration = Utilities.multiplyVector(1 / massOfBall, netForce);

                            //Euler's method
                            double start_time = 0.1;
                            double h = 0.1;

                            boolean end = false;

                            Vector currentPosition = initialPosition;
                            Vector currentVelocity = initialVelocity;
                            Vector currentAcceleration = acceleration;

                            System.out.println("Initial Position    : " + currentPosition);
                            System.out.println("Initial Velocity    : " + currentVelocity);
                            System.out.println("Initial Acceleration: " + currentAcceleration);
                            System.out.println("Wind:" + wind);
                            System.out.println("Spin vector: " + spinPseudoVector);
                            System.out.println("");

                            for(double time = start_time ; end == false; time += h)
                            {
                                time = (double)Math.round(time * 100) / 100;
                                currentPosition = Utilities.addVector(currentPosition, Utilities.multiplyVector(h, currentVelocity));
                                currentVelocity = Utilities.addVector(currentVelocity, Utilities.multiplyVector(h, currentAcceleration));

                                Vector newApparentVelocity = Utilities.minusVector(currentVelocity, wind);

                                Vector newDragForce = Utilities.getForceOfDrag(densityOfFluid, ballSurfaceArea, dragCoef, newApparentVelocity);
                                Vector newMagnusForce = Utilities.getMagnusForce(densityOfFluid, ballSurfaceArea, radiusOfBall, newApparentVelocity, spinPseudoVector);
                                Vector newNetForce = Utilities.addVector(Utilities.addVector(gForce, newDragForce), newMagnusForce);

                                currentAcceleration = Utilities.multiplyVector(1 / massOfBall, newNetForce);
                                System.out.println("Current time: " + time);
                                System.out.println("Position    : " + currentPosition);
                                System.out.println("Velocity    : " + currentVelocity);
                                System.out.println("Acceleration: " + currentAcceleration);
                                System.out.println("Drag force: " + newDragForce);
                                System.out.println("Magnus force: " + newMagnusForce);
                                System.out.println("");

                                if(currentPosition.getk() < 0)
                                {
                                    end = true;
                                    System.out.println("Opps, the ball hits the ground around this time! ( " + time + "s )");
                                }
                            }
                            break;
                            
                        case 4:
                            end_program = true;
                            System.out.println("Bye!");
                            break;
                    }
                }
                else
                {
                    i = 0;
                    in.nextLine();
                    System.out.println("Only numbers between 1 to 4 is accepted!");
                }
            }
        }
        catch (InputMismatchException e)
        {
            System.out.println("Opps, looks like what u entered is not a number!");
        }
    }
}
