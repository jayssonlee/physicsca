public class Utilities 
{
    public static double getSphereMass(double radius, double density)
    {
        double mass = ( 4 * Math.PI * radius * radius * radius * density ) / 3;
        
        return mass;
    }
    
    public static Vector addVector(Vector a, Vector b)
    {
        double iVal = a.geti() + b.geti();
        double jVal = a.getj() + b.getj();
        double kVal = a.getk() + b.getk();
        
        Vector addVector = new Vector(iVal, jVal, kVal);
        
        return addVector;
    }
    
    public static Vector minusVector(Vector a, Vector b)
    {
        double iVal = a.geti() - b.geti();
        double jVal = a.getj() - b.getj();
        double kVal = a.getk() - b.getk();
        
        Vector minusVector = new Vector(iVal, jVal, kVal);
        
        return minusVector;
    }
    
    public static Vector multiplyVector(double multiple, Vector a)
    {
        double iVal = multiple * a.geti();
        double jVal = multiple * a.getj();
        double kVal = multiple * a.getk();
        
        Vector minusVector = new Vector(iVal, jVal, kVal);
        
        return minusVector;
    }
    
    public static Vector crossProduct(Vector a, Vector b)
    {
        double iVal = ( a.getj() * b.getk() ) - ( b.getj() * a.getk() );
        double jVal = ( a.geti() * b.getk() ) - ( b.geti() * a.getk() );
        double kVal = ( a.geti() * b.getj() ) - ( b.geti() * a.getj() );
        
        Vector crossVector = new Vector(iVal, jVal, kVal);
        
        return crossVector;
    }
    
    public static double dotProduct(Vector a)
    {
        double dotproduct = Math.sqrt( Math.pow(a.geti(), 2) + Math.pow(a.getj(), 2) + Math.pow(a.getk(), 2) );
        
        return dotproduct;
    }
    
    public static Vector getForceOfGravity(double mass)
    {
        double iVal = 0;
        double jVal = 0;
        double kVal = mass * (-9.81);
        
        Vector gForce = new Vector(iVal, jVal, kVal);
        
        return gForce;
    }
    
    public static Vector getUnitVector(Vector vec)
    {
        double length = dotProduct(vec);
        
        double iVal = ( (double)1 / length ) * vec.geti();
        double jVal = ( (double)1 / length ) * vec.getj();
        double kVal = ( (double)1 / length ) * vec.getk();
        
        Vector unitVector = new Vector(iVal, jVal, kVal);
        
        return unitVector;
    }
    
    public static Vector getForceOfDrag(double densityOfFluid, double surfaceArea, double coefDrag, Vector appVelocity)
    {
        double lengthOfVelocity = dotProduct(appVelocity);
        
        double iVal = -0.5 * densityOfFluid * surfaceArea * coefDrag * lengthOfVelocity * appVelocity.geti();
        double jVal = -0.5 * densityOfFluid * surfaceArea * coefDrag * lengthOfVelocity * appVelocity.getj();
        double kVal = -0.5 * densityOfFluid * surfaceArea * coefDrag * lengthOfVelocity * appVelocity.getk();
        
        Vector dragForce = new Vector(iVal, jVal, kVal);
        
        return dragForce;
    }
    
    public static Vector getMagnusForce(double densityOfFluid, double surfaceArea, double radius, Vector appVelocity, Vector pseudoVector)
    {
        double omega = dotProduct(pseudoVector);
        double lengthOfVelocity = dotProduct(appVelocity);

        Vector fm = crossProduct(pseudoVector, appVelocity);
        
        Vector unitFm = getUnitVector(fm);
        
        double iVal = 0.5 * densityOfFluid * surfaceArea * radius * omega * lengthOfVelocity * unitFm.geti();
        double jVal = 0.5 * densityOfFluid * surfaceArea * radius * omega * lengthOfVelocity * unitFm.getj();
        double kVal = 0.5 * densityOfFluid * surfaceArea * radius * omega * lengthOfVelocity * unitFm.getk();
        
        Vector magnusForce = new Vector(iVal, jVal, kVal);
        
        return magnusForce;
    }
}
